import { addClass, removeClass } from "./domHelper.mjs";

const username = sessionStorage.getItem("username");
let numOfLetters = 0;
let enteredNumOfLetters = 0;
let misclicked_counter = 0;

if (!username) {
  window.location.replace("/login");
}

document.getElementById("title_username").innerHTML = username;
const socket = io("", { query: { username } });

document.getElementById("add-room-btn").addEventListener("click", () => {
  const room_name = prompt("Enter room name");
  if (room_name) {
    socket.emit('createRoom', room_name);
  }
});

document.getElementById("quit-room-btn").addEventListener("click", () => {
  socket.emit('leaveRoom');
  removeClass(document.getElementById('rooms-page'), 'display-none');
  addClass(document.getElementById('game-page'), 'display-none');
});

const ReadyButton = document.getElementById("ready-btn");
ReadyButton.addEventListener("click", () => {
  if (ReadyButton.innerHTML === 'Ready') {
    ReadyButton.innerHTML = 'Not Ready'
  }
  else {
    ReadyButton.innerHTML = 'Ready'
  }
  socket.emit('Ready');
});

function eventJoinRoomBtn(e) {
  socket.emit('joinRoom', e.target.getAttribute('room_id'));
}

function MakeUserStr(user) {
  const status = user.isReady ? "green" : "red";
  let isYou = "";
  if (user.username === username) {
    isYou = "(you)"
  }
  return `
      <div id="user-${user.username}">
      <div class="user_info">
      <div class="ready-status-${status}" id="ready-status-${user.username}"></div>
      ${user.username} ${isYou}
      </div>
      <div class="bar">
      <div class="user-progress ${user.username}"></div>
      </div>
      </div>`
}

function UpdateRoom(user, isAdd) {
  const usersElement = document.getElementById("users");
  let str = "";
  if (isAdd) {
    str += MakeUserStr(user);
    usersElement.innerHTML += str;
  }
  else {
    usersElement.removeChild(document.getElementById(`user-${user.username}`))
  }
}

function AddSpanToEveryLetter(text) {
  const letterArr = text.split("");
  let newText = "";
  letterArr.forEach((letter, index) => {
    let isNext = ""
    if (index === 0) {
      isNext = "next";
    }
    newText += `<span class="not-entered ${isNext}">${letter}</span>`
  })
  numOfLetters = letterArr.length;
  return newText;
}


function onKeyDown(e) {
  if (e.keyCode == 32) {
    e.preventDefault();
  }
  const letterElements = document.getElementsByClassName('not-entered');
  if (letterElements[0]) {
    if (e.key == letterElements[0].innerHTML) {
      if (letterElements[1]) {
        addClass(letterElements[1], "next");
      }
      addClass(letterElements[0], "entered");
      removeClass(letterElements[0], "next");
      removeClass(letterElements[0], "not-entered");
      ++enteredNumOfLetters;
      socket.emit("Change progress bar", numOfLetters, enteredNumOfLetters, username);
    }
    else if (e.keyCode !== 16) {
      ++misclicked_counter;
      document.getElementById("misclicked-counter").innerHTML = misclicked_counter;
    }
  }
}

socket.on('AddRooms', rooms_name => {
  let src = "";
  for (const key in rooms_name) {
    let style = ""
    if (!rooms_name[key].isShow) {
      style = 'style="display: none"'
    }
    src += `
    <div class="room" id="${key}" ${style}>
    <div class="users_connected">
    <div id="count_users_${key}">${rooms_name[key].usersCount}</div>
    <span> users connected</span>
    </div>
    <div class="room_name">${key}</div>
    <button class="join-room-btn" room_id="${key}">Join</button>
    </div>
    `;
  }
  document.getElementById('rooms').innerHTML += src;
  document.querySelectorAll(".join-room-btn").forEach(element => {
    element.addEventListener("click", eventJoinRoomBtn)
  })
})

socket.on('UserExists', () => {
  window.alert("User with this name is already exists");
  sessionStorage.removeItem("username");
  window.location.replace("/login");
})

socket.on('RoomExists', () => {
  window.alert("Room with this name is already exists");
})

socket.on('RoomCreated', room_name => {
  document.querySelectorAll(".join-room-btn").forEach(element => {
    element.removeEventListener("click", eventJoinRoomBtn)
  })
  document.getElementById('rooms').innerHTML += `
  <div class="room" id="${room_name}">
  <div class="users_connected">
  <div id="count_users_${room_name}">1</div>
  <span> users connected</span>
  </div>
  <div class="room_name">${room_name}</div>
  <button class="join-room-btn" room_id="${room_name}">Join</button>
  </div>
  `;
  document.querySelectorAll(".join-room-btn").forEach(element => {
    element.addEventListener("click", eventJoinRoomBtn)
  })
})

socket.on('JoinRoom', (users, roomId) => {
  addClass(document.getElementById('rooms-page'), 'display-none');
  removeClass(document.getElementById('game-page'), 'display-none');
  document.getElementById("game-page-room-name").innerHTML = roomId;
  const usersElement = document.getElementById("users");
  let str = "";
  users.forEach(user => {
    str += MakeUserStr(user);
  });
  usersElement.innerHTML += str;
}
)

socket.on("ClearRoom", () => {
  document.getElementById("users").innerHTML = "";
})

socket.on('UpdateRoom', (user, isAdd) => {
  UpdateRoom(user, isAdd);
})

socket.on('ChangeUsersAmount', (room_name, user_amount) => {
  document.getElementById(`count_users_${room_name}`).innerHTML = user_amount;
})

socket.on('DeleteRoom', room_name => {
  document.getElementById('rooms').removeChild(document.getElementById(room_name));
})


socket.on('UserReady', (username, isReady) => {
  const ready_status = document.getElementById(`ready-status-${username}`);
  if (isReady) {
    removeClass(ready_status, 'ready-status-red');
    addClass(ready_status, 'ready-status-green');
  }
  else {
    addClass(ready_status, 'ready-status-red');
    removeClass(ready_status, 'ready-status-green');
  }
})

socket.on('HideRoom', room_name => {
  document.getElementById(room_name).style.display = 'none';
})

socket.on('ShowRoom', room_name => {
  document.getElementById(room_name).style.display = 'grid';
})

socket.on('Start timer', () => {
  document.getElementById("ready-btn").innerHTML = "Ready";
  addClass(document.getElementById("ready-btn"), 'display-none');
  addClass(document.getElementById("quit-room-btn"), 'display-none');
  removeClass(document.getElementById("timer"), 'display-none');
  socket.emit("Timer");
})

socket.on('Timer', newValue => {
  document.getElementById("timer").innerHTML = newValue;
})

socket.on('Game timer', newValue => {
  document.getElementById("game-timer").innerHTML = newValue;
})

socket.on('Start game', async randId => {
  numOfLetters = 0;
  enteredNumOfLetters = 0;
  addClass(document.getElementById("timer"), 'display-none');
  document.getElementById("text-container").style.display = 'block';
  document.getElementById("misclicked").style.display = 'block';
  document.getElementById("left-time").style.display = "flex";
  const randText = await (await fetch(`/game/texts/${randId}`)).text();
  document.getElementById("text-container").innerHTML = AddSpanToEveryLetter(randText);
  document.addEventListener('keydown', onKeyDown);
})

socket.on("Change progress bar", (bar_width, username) => {
  const bar = document.querySelector(`.user-progress.${username}`);
  if (bar_width >= 100) {
    bar.style.background = 'green';
    bar.style.width = '100%';
  }
  else {
    bar.style.width = `${bar_width}%`;
  }
})

socket.on("Game end", users => {
  removeClass(document.getElementById("quit-room-btn"), "display-none");
  removeClass(document.getElementById("ready-btn"), "display-none");
  users.forEach(user => {
    const ready_status = document.getElementById(`ready-status-${user.username}`);
    addClass(ready_status, 'ready-status-red');
    removeClass(ready_status, 'ready-status-green');
  })
  document.getElementById("left-time").style.display = "none";
  document.getElementById("text-container").style.display = 'none';
  document.getElementById("misclicked").style.display = 'none';
  document.getElementById("misclicked-counter").innerHTML = 0;
  document.removeEventListener('keydown', onKeyDown);
  document.querySelectorAll(`.user-progress`).forEach(bar => {
    bar.style.width = 0;
  });
  document.getElementById("game-timer").innerHTML = "";
  document.getElementById("timer").innerHTML = "";
  socket.emit("StopGameTimer");
  misclicked_counter = 0;
})

socket.on("Set commentator phrase", phrase => {
  document.getElementById("commentator-text").innerHTML = phrase;
})
