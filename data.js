export const texts = [
  "Being human makes us susceptible to the onset of feelings. The role of these emotions varies. Some of them are useful while others may be harmful. The use of social media for self-expression has reached a point that it makes us feel we can say anything.",
  "This begins when we see people expressing anything and everything that come to mind. When we see everyone else voicing their likes and dislikes, their irritations and desires we tend to imitate what they do. And because many engage in this, we think that it is normal and healthy.",
  "The basic technique stands in contrast to hunt and peck typing in which the typist keeps this or her eyes on the source copy at all times. Touch typing also involves the use of the home row method, where typists keep their wrists up, rather than resting them on a desk or keyboard (which can cause carpal tunnel syndrome). ",
  "Trying to make a wise, good choice when thinking about what kinds of careers might be best for you is a hard thing to do. Your future may very well depend on the ways you go about finding the best job openings for you in the world of work. Some people will feel that there is one and only one job in the world for them.",
  "A paralegal is a person trained in legal matters who performs tasks requiring knowledge of the law and legal procedures. A paralegal is not a lawyer but can be employed by a law office or work freelance at a company or law office. Paralegals are not allowed to offer legal services directly to the public on their own and must perform their legal work under an attorney or law firm.",
  "Self-confidence is a tricky subject for many people.For some, it's impossible to feel good about themselves without outside validation. When you're in a situation where the people in your life aren't helping you to feel better about yourself, this can become a problem in your day to day life. Most insecurity stems from feelings of not being attractive or feelings of loneliness."
];

export const commentatorsPhrases = {
  start: [
    "Доброго Вам времени суток, господа, и отличной игры!",
    "Все приготовились и зарядились на прекрасную игру!!!",
    "Хорошей игры и пусть удача всегда будет на вашей стороне!",],
  greeting: [
    "Приветствую {username}!",
    "Добро пожаловать {username}!",
    "Как поживаешь {username}!",
    "Доброго времени суток {username}"
  ],
  parting: [
    "Увидимся позже {username}",
    "До скорой встречи {username}, моя любовь к тебе навечно",
    "Прощай {username}",
    "Всего хорошего {username}!"
  ],
  every30sec: [
    "На первом месте {username1}, на втором {username2} за ним идет {username3}",
    "Результаты на данный момент гонки: #1 {username1}, #2 {username2}, #3 {username3}"
  ],
  before30symbols: [
    "До финиша осталось совсем немного и похоже что первым его может пересечь {username}. Но давайте дождемся финиша",
    "{username} поднажми осталось совсем чуть-чуть"],
  finish: [
    "Финишную прямую пересекает {username}",
    "Завершает круг {username}",
    "{username} напечатал весь текст!"
  ],
  end: [
    "Вот гонка и закончилась! Все молодцы и финальный результат: первое место занимает {username1}, на втором {username2} за ним идет {username3}",
    "Конец! Поздравляем всех, а вот тройка победителей: #1 {username1}, #2 {username2}, #3 {username3}"
  ],
  jokes: [
    "Если посеять зерно вырастет пшено. Если повесить ключи вырастет дом",
    "Прихожу домой, смотрю пыль лежит та и думаю и я полежу",
    "Кто рано встает, тот скорее всего не выспался",
    "Не можешь противостоять - противоложись",
    "Копчик это американский маленький полицейский",
    "Когда я вышел на улицу, шанс встретить динозавра был 50%. Либо встречу, либо нет.",
    "Не важно кто справа, а важно кто слива",
    "Виноград взрывается в микроволновой печи.",
    "Дельфины спят с одним открытым глазом.",
    "Игрушка йо-йо появилась в 16-м веке на Филиппинах в качестве оружия.",
    "Комаров привлекает запах людей, которые недавно ели бананы.",
    "В Индии игральные карты круглые",
    "Улитка может спать до трех лет."
  ]
}

export default { texts, commentatorsPhrases };
