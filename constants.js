const START = 'start'
const GREETING = 'greeting'
const PARTING = 'parting'
const EVERY30SEC = 'every30sec'
const BEFORE30SYMBOLS = 'before30symbols'
const FINISH = 'finish'
const END = 'end'
const JOKES = 'jokes'
const NOBODY = "никого"
const AMOUNT_BEFORE_SYMBOLS = 30
const typesOfPhrases = { START, GREETING, EVERY30SEC, BEFORE30SYMBOLS, FINISH, END, JOKES, PARTING };
export { typesOfPhrases, NOBODY, AMOUNT_BEFORE_SYMBOLS }