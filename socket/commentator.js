import { commentatorsPhrases } from "../data"
import { typesOfPhrases } from '../constants'

class commentatorsPhrase { //facade
    constructor(type) {
        this.type = type
    }

    getRand() {
        let phrase;
        if (this.type === typesOfPhrases.START) {
            phrase = this._getRandFromArr(commentatorsPhrases.start);
        } else if (this.type === typesOfPhrases.GREETING) {
            phrase = this._getRandFromArr(commentatorsPhrases.greeting);
        } else if (this.type === typesOfPhrases.PARTING) {
            phrase = this._getRandFromArr(commentatorsPhrases.parting);
        } else if (this.type === typesOfPhrases.EVERY30SEC) {
            phrase = this._getRandFromArr(commentatorsPhrases.every30sec);
        } else if (this.type === typesOfPhrases.BEFORE30SYMBOLS) {
            phrase = this._getRandFromArr(commentatorsPhrases.before30symbols);
        } else if (this.type === typesOfPhrases.FINISH) {
            phrase = this._getRandFromArr(commentatorsPhrases.finish);
        } else if (this.type === typesOfPhrases.END) {
            phrase = this._getRandFromArr(commentatorsPhrases.end);
        } else if (this.type === typesOfPhrases.JOKES) {
            phrase = this._getRandFromArr(commentatorsPhrases.jokes);
        }
        return phrase;
    }

    _getRandFromArr(arr) {
        return arr[Math.floor(Math.random() * arr.length)]
    }
}

class phraseProxy { //proxy
    constructor(type) {
        if (Object.values(typesOfPhrases).includes(type)) {
            this.phrase = new commentatorsPhrase(type);
        }
        else {
            console.error("No such phrase type");
        }
    }
    getText() {
        if (this.phrase) {
            return this.phrase.getRand();
        }
        console.error("No phrase")
    }
}

export { phraseProxy };