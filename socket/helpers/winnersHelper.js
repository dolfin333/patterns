function getWinners(winners) { //pure function
    let arr = [];
    for (const key in winners) {
        arr.push({ username: key, ...winners[key] })
    }
    let map = arr.reduce((r, i) => {
        r[i.bar_width] = r[i.bar_width] || [];
        r[i.bar_width].push(i);
        return r;
    }, {});
    arr = [];
    for (let key in map) {
        arr.push(map[key]);
    }
    arr = arr.map(users => {
        users.sort(function (a, b) {
            return new Date(a.time) - new Date(b.time);
        });
        return users;
    })
    arr.sort(function (a, b) {
        return b[0].bar_width - a[0].bar_width;
    });
    const merged = [].concat.apply([], arr);
    return merged;
}

export { getWinners }