const stringHelper = {
    create: (function () {
        const regexp = /{([^{]+)}/g;

        return function (str) { //currying
            return function (o) {
                return str.replace(regexp, function (ignore, key) {
                    return (key = o[key]) == null ? '' : key;
                });
            }
        }
    })()
};

export { stringHelper }