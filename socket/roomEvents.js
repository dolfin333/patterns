import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "./config";
import { texts } from "../data";
import { phraseProxy } from "./commentator";
import { typesOfPhrases, NOBODY, AMOUNT_BEFORE_SYMBOLS } from '../constants'
import { getWinners } from "./helpers/winnersHelper";
import { stringHelper } from "./helpers/stringHelper"

const usernames = [];
const activeRoomnames = {};
const roomsTextIds = {};
const rooms = {};
const winners = {};

const createUser = ({ //factory function
    username = "",
    isReady = false,
    isEnd = false
} = {}) => ({
    username,
    isReady,
    isEnd
});

function getRandomTextId() {
    return Math.floor(Math.random() * (texts.length))
}

function EveryoneIsReady(users) { //pure function
    return users.every(user => {
        return user.isReady;
    });
}

function getBarWidth(numOfLetters) { //currying function
    return function (enteredNumOfLetters) {
        return (100 * enteredNumOfLetters) / numOfLetters
    };
}


const getRoomEvents = (io, socket) => {
    const username = socket.handshake.query.username;
    let currentRoom = null;
    let isRoomShow = false;
    let isReady = false;
    let isGameOn = false;
    let gameTimer;
    let timer;

    if (usernames.indexOf(username) !== -1) {
        socket.emit('UserExists');
    }
    else {
        usernames.push(username);
        socket.emit('AddRooms', activeRoomnames)
    }

    function HideRoom() {
        activeRoomnames[currentRoom].isShow = false;
        io.sockets.emit("HideRoom", currentRoom);
        isRoomShow = false;
    }

    function ShowRoom() {
        activeRoomnames[currentRoom].isShow = true;
        io.sockets.emit("ShowRoom", currentRoom);
        isRoomShow = true;
    }

    function ifEveryoneIsReady() {
        if (currentRoom) {
            roomsTextIds[currentRoom] = getRandomTextId();
            rooms[currentRoom] = rooms[currentRoom].map(user => {
                user.isReady = false;
                return user;
            });
            isReady = false;
            isGameOn = true;
            io.to(currentRoom).emit("Start timer");
            io.to(currentRoom).emit("Set commentator phrase", (new phraseProxy(typesOfPhrases.START)).getText());
            HideRoom();
        }
    }

    function GameEnd() {
        rooms[currentRoom] = rooms[currentRoom].map(user => {
            user.isEnd = false;
            return user
        });
        isGameOn = false;
        if (rooms[currentRoom].length !== MAXIMUM_USERS_FOR_ONE_ROOM) {
            ShowRoom()
        }
        const usersWin = getWinners(winners[currentRoom]);
        io.to(currentRoom).emit("Set commentator phrase", stringHelper.create((new phraseProxy(typesOfPhrases.END)).getText())({
            username1: usersWin[0].username,
            username2: usersWin[1]?.username ?? NOBODY,
            username3: usersWin[2]?.username ?? NOBODY
        }));
        io.to(currentRoom).emit("Game end", rooms[currentRoom]);
    }

    function startGameTimer() {
        let counter = SECONDS_FOR_GAME;
        socket.emit('Game timer', counter);
        gameTimer = setInterval(function () {
            counter--;
            socket.emit('Game timer', counter);
            if (counter % 30 === 0) {
                const usersWin = getWinners(winners[currentRoom]);
                socket.emit("Set commentator phrase",
                    stringHelper.create((new phraseProxy(typesOfPhrases.EVERY30SEC)).getText())({
                        username1: usersWin[0].username,
                        username2: usersWin[1]?.username ?? NOBODY,
                        username3: usersWin[2]?.username ?? NOBODY
                    }));
            }
            else if (counter % 10 === 0) {
                io.to(currentRoom).emit("Set commentator phrase", (new phraseProxy(typesOfPhrases.JOKES)).getText());
            }
            if (counter === 0) {
                clearInterval(gameTimer);
                GameEnd();
            }
        }, 1000);
    }

    function stopGameTimer() {
        clearInterval(gameTimer);
    }

    function stopTimer() {
        clearInterval(timer);
    }

    function LeaveRoom() {
        rooms[currentRoom] = rooms[currentRoom].filter(user => user.username !== username);
        activeRoomnames[currentRoom].usersCount = activeRoomnames[currentRoom].usersCount - 1;
        if (winners[currentRoom]) {
            delete winners[currentRoom][username];
        }
        if (rooms[currentRoom].length === 0) {
            delete activeRoomnames[currentRoom]
            delete rooms[currentRoom];
            if (winners[currentRoom]) {
                delete winners[currentRoom];
            }
            io.sockets.emit("DeleteRoom", currentRoom);
        }
        else {
            socket.to(currentRoom).emit("UpdateRoom", createUser({ username: username, isReady: isReady }), false);
            io.sockets.emit("ChangeUsersAmount", currentRoom, rooms[currentRoom].length);
            if (rooms[currentRoom].every(user => user.isEnd === true)) {
                GameEnd();
            }
            if (!isGameOn) {
                ShowRoom();
            }
            if (EveryoneIsReady(rooms[currentRoom])) {
                ifEveryoneIsReady();
            }
        }
        io.to(currentRoom).emit("Set commentator phrase", stringHelper.create((new phraseProxy(typesOfPhrases.PARTING)).getText())({ username }));
        socket.emit("ClearRoom");
        socket.leave(currentRoom);
        currentRoom = null;
        isGameOn = false;
        isReady = false;
        stopGameTimer();
        stopTimer();
    }
    return {
        stopGameTimer,
        LeaveRoom,
        createRoom: (roomId) => {
            if (rooms[roomId]) {
                socket.emit("RoomExists");
            } else {
                socket.join(roomId);
                currentRoom = roomId;
                activeRoomnames[roomId] = { usersCount: 1, isShow: true };
                rooms[roomId] = [createUser({ username: username, isReady: isReady, isEnd: false })];
                socket.emit("JoinRoom", rooms[roomId], roomId);
                io.to(roomId).emit("Set commentator phrase", stringHelper.create((new phraseProxy(typesOfPhrases.GREETING)).getText())({ username }));
                io.sockets.emit("RoomCreated", roomId);
                isRoomShow = true;
            }
        },
        joinRoom: (roomId) => {
            currentRoom = roomId;
            socket.join(roomId);
            const user = createUser({ username: username, isReady: isReady, isEnd: false });
            rooms[roomId].push(user);
            activeRoomnames[roomId].usersCount = activeRoomnames[roomId].usersCount + 1;
            socket.emit("JoinRoom", rooms[roomId], roomId);
            io.to(roomId).emit("Set commentator phrase", (stringHelper.create((new phraseProxy(typesOfPhrases.GREETING)).getText())({ username })));
            socket.to(roomId).emit("UpdateRoom", user, true);
            io.sockets.emit("ChangeUsersAmount", roomId, rooms[roomId].length);
            if (rooms[roomId].length === MAXIMUM_USERS_FOR_ONE_ROOM) {
                HideRoom();
            }
        },
        Ready: () => {
            isReady = !isReady;
            rooms[currentRoom] = rooms[currentRoom].map(user => {
                if (user.username === username) {
                    user.isReady = !user.isReady
                }
                return user
            })
            io.to(currentRoom).emit("UserReady", username, isReady);
            if (EveryoneIsReady(rooms[currentRoom])) {
                ifEveryoneIsReady();
            }
        },

        Timer: () => {
            isReady = false;
            let counter = SECONDS_TIMER_BEFORE_START_GAME;
            io.to(currentRoom).emit('Timer', counter);
            timer = setInterval(function () {
                counter--;
                io.to(currentRoom).emit('Timer', counter);
                if (counter === 0) {
                    io.to(currentRoom).emit('setRandText', roomsTextIds[currentRoom]);
                    winners[currentRoom] = {};
                    rooms[currentRoom].forEach(user => {
                        winners[currentRoom][user.username] = { bar_width: 0, time: 0 };
                    })
                    isGameOn = true;
                    socket.emit("Start game", roomsTextIds[currentRoom]);
                    startGameTimer();
                    clearInterval(timer);
                }
            }, 1000);
        },
        ChangeProgressBar: (numOfLetters, enteredNumOfLetters, username) => {
            const bar_width = getBarWidth(numOfLetters)(enteredNumOfLetters);
            winners[currentRoom][username] = { bar_width: bar_width, time: new Date };
            io.to(currentRoom).emit('Change progress bar', bar_width, username);
            if (numOfLetters - enteredNumOfLetters === AMOUNT_BEFORE_SYMBOLS) {
                io.to(currentRoom).emit("Set commentator phrase", stringHelper.create((new phraseProxy(typesOfPhrases.BEFORE30SYMBOLS)).getText())({ username }));
            }
            if (bar_width >= 100) {
                rooms[currentRoom] = rooms[currentRoom].map(user => {
                    if (user.username === username) {
                        user.isEnd = true;
                    }
                    return user
                })
                io.to(currentRoom).emit("Set commentator phrase", stringHelper.create((new phraseProxy(typesOfPhrases.FINISH)).getText())({ username }));
            }
            if (rooms[currentRoom].every(user => user.isEnd === true)) {
                stopGameTimer();
                GameEnd();
            }
        },
        Disconnect: () => {
            if (currentRoom) {
                LeaveRoom(currentRoom);
                stopGameTimer();
                stopTimer();
            }
            usernames.splice(usernames.indexOf(username), 1);
        }
    }
}


export { getRoomEvents };